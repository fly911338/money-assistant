package com.kevin.moneyassistant.repository;

import com.kevin.moneyassistant.entiry.MoneyRecord;
import lombok.AllArgsConstructor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.text.SimpleDateFormat;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class MoneyRecordRepositoryTest {

    @Autowired
    private MoneyRecordRepository underTest;

    @Test
    void itShouldQueryByRecordsDate() {
        // Given
        var date = "2021-04-22";
        underTest.save(new MoneyRecord(null, "便當", 1L, 105.0, date, null, null, null));
        underTest.save(new MoneyRecord(null, "飲料", 1L, 105.0, date, null, null, null));
        underTest.save(new MoneyRecord(null, "小費", 1L, 105.0, date, null, null, null));
        // When
        var list = underTest.findByRecordDate(date);
        // Then
        assertThat(list).isPresent();
    }
}
