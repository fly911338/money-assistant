package com.kevin.moneyassistant.service;

import com.kevin.moneyassistant.entiry.MoneyRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class MoneyRecordServiceImplTest {

    @Autowired
    private MoneyRecordService underTest;

    @Test
    void itShouldAddNewRecord() {
        // Given
        // When
        // Then
    }

    @Test
    void itShouldModifyRecord() {
        // Given
        // When
        // Then
    }

    @Test
    void itShouldQueryByRecordsDate() {
        // Given
        var date = "2021-04-22";
        MoneyRecord record1 = new MoneyRecord(null, "便當", 1L, 105.0, date, Instant.now(), Instant.now(), null);
        MoneyRecord record2 = new MoneyRecord(null, "飲料", 1L, 60.0, date, Instant.now(), Instant.now(), null);
        MoneyRecord record3 = new MoneyRecord(null, "小費", 1L, 15.0, date, Instant.now(), Instant.now(), null);
        underTest.addNewRecord(record1);
        underTest.addNewRecord(record2);
        underTest.addNewRecord(record3);
        // When
        var result = underTest.findByRecordDate(date).get();
        // Then  usingRecursiveCompariso
        assertThat(record1).usingRecursiveComparison();
    }
}
