package com.kevin.moneyassistant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoneyAssistantApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoneyAssistantApplication.class, args);
	}

}
