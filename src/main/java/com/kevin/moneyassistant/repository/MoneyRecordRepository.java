package com.kevin.moneyassistant.repository;

import com.kevin.moneyassistant.entiry.MoneyRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
public interface MoneyRecordRepository extends JpaRepository<MoneyRecord, Long> {

    Optional<List<MoneyRecord>> findByRecordDate(String date);
}
