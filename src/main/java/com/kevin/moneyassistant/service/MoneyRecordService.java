package com.kevin.moneyassistant.service;

import com.kevin.moneyassistant.entiry.MoneyRecord;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;


public interface MoneyRecordService {

    void addNewRecord(MoneyRecord moneyRecord);
    void modifyRecord(MoneyRecord moneyRecord);
    Optional<List<MoneyRecord>> findByRecordDate(String date);

}
