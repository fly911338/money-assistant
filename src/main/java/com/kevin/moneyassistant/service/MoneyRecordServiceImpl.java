package com.kevin.moneyassistant.service;

import com.kevin.moneyassistant.entiry.MoneyRecord;
import com.kevin.moneyassistant.repository.MoneyRecordRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MoneyRecordServiceImpl implements MoneyRecordService{

    private final MoneyRecordRepository moneyRecordRepository;

    @Override
    public void addNewRecord(MoneyRecord moneyRecord) {
        moneyRecordRepository.save(moneyRecord);
    }

    @Override
    public void modifyRecord(MoneyRecord moneyRecord) {
    }

    @Override
    public Optional<List<MoneyRecord>> findByRecordDate(String date) {
        return moneyRecordRepository.findByRecordDate(date);
    }
}
