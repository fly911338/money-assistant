# Money assistant side project

當前在用一些記帳軟體有些功能要pro版才能使用， 因此想說自己寫寫看，<br>
試著導入一些我覺得實務上需要的功能用web形式呈現（因為我不會寫app😅😅😅)，<br>
並且練習CICD自動部署到aws上面。

## 目前預計架構V0.1
兩個頁面
1.行事曆頁面呈現當月各日期點選會跳出當日明細
2.紀錄每筆明細交易紀錄事項頁面

## 目前預計使用技術
### front-end
angular
### back-end
java, maven, spring-boot, MYSQL
### dev-ops 
aws-ec2, jenkins

